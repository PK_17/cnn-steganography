from keras.models import Model
from keras.layers import Input
from keras.layers.convolutional import Conv2D
from keras.utils import plot_model
from keras.layers.merge import concatenate
from keras.models import model_from_json
from Transformer import ycbcr_to_rgb, block_wise_idct
from Preprocessor import Preprocessor


class DCTSteganographyModel:

    def __init__(self, dct_secret, dct_cover, n_epochs, beta):
        self.dct_secret = Input(shape=(Preprocessor.IMAGE_SIZE[0], Preprocessor.IMAGE_SIZE[1], dct_secret.shape[3]))
        self.dct_cover = Input(shape=(Preprocessor.IMAGE_SIZE[0], Preprocessor.IMAGE_SIZE[1], dct_cover.shape[3]))

        self.beta = beta
        self.n_epochs = n_epochs
        self.model = self.get_model()

    def get_preparation_network(self):
        # 3x3 branch
        conv2d_3x3_1 = Conv2D(filters=50, kernel_size=3, padding='same', activation='relu')(self.dct_secret)
        conv2d_3x3_2 = Conv2D(filters=50, kernel_size=3, padding='same', activation='relu')(conv2d_3x3_1)
        conv2d_3x3_3 = Conv2D(filters=50, kernel_size=3, padding='same', activation='relu')(conv2d_3x3_2)
        conv2d_3x3_4 = Conv2D(filters=50, kernel_size=3, padding='same', activation='relu')(conv2d_3x3_3)

        # 4x4 branch
        conv2d_4x4_1 = Conv2D(filters=50, kernel_size=4, padding='same', activation='relu')(self.dct_secret)
        conv2d_4x4_2 = Conv2D(filters=50, kernel_size=4, padding='same', activation='relu')(conv2d_4x4_1)
        conv2d_4x4_3 = Conv2D(filters=50, kernel_size=4, padding='same', activation='relu')(conv2d_4x4_2)
        conv2d_4x4_4 = Conv2D(filters=50, kernel_size=4, padding='same', activation='relu')(conv2d_4x4_3)

        # 5x5 branch
        conv2d_5x5_1 = Conv2D(filters=50, kernel_size=5, padding='same', activation='relu')(self.dct_secret)
        conv2d_5x5_2 = Conv2D(filters=50, kernel_size=5, padding='same', activation='relu')(conv2d_5x5_1)
        conv2d_5x5_3 = Conv2D(filters=50, kernel_size=5, padding='same', activation='relu')(conv2d_5x5_2)
        conv2d_5x5_4 = Conv2D(filters=50, kernel_size=5, padding='same', activation='relu')(conv2d_5x5_3)

        # First concatenation
        concat_1 = concatenate([conv2d_3x3_4, conv2d_4x4_4, conv2d_5x5_4])

        # Pre-output branching
        conv2d_3x3_final = Conv2D(filters=50, kernel_size=3, padding='same', activation='relu')(concat_1)
        conv2d_4x4_final = Conv2D(filters=50, kernel_size=4, padding='same', activation='relu')(concat_1)
        conv2d_5x5_final = Conv2D(filters=50, kernel_size=5, padding='same', activation='relu')(concat_1)

        # Final concatenation
        prepared_secret = concatenate([conv2d_3x3_final, conv2d_4x4_final, conv2d_5x5_final])

        return prepared_secret

    def get_hiding_network(self, prepared_secret):
        input_concat = concatenate([prepared_secret, self.dct_cover])

        # 3x3 branch
        conv2d_3x3_1 = Conv2D(filters=50, kernel_size=3, padding='same', activation='relu')(input_concat)
        conv2d_3x3_2 = Conv2D(filters=50, kernel_size=3, padding='same', activation='relu')(conv2d_3x3_1)
        conv2d_3x3_3 = Conv2D(filters=50, kernel_size=3, padding='same', activation='relu')(conv2d_3x3_2)
        conv2d_3x3_4 = Conv2D(filters=50, kernel_size=3, padding='same', activation='relu')(conv2d_3x3_3)

        # 4x4 branch
        conv2d_4x4_1 = Conv2D(filters=50, kernel_size=4, padding='same', activation='relu')(input_concat)
        conv2d_4x4_2 = Conv2D(filters=50, kernel_size=4, padding='same', activation='relu')(conv2d_4x4_1)
        conv2d_4x4_3 = Conv2D(filters=50, kernel_size=4, padding='same', activation='relu')(conv2d_4x4_2)
        conv2d_4x4_4 = Conv2D(filters=50, kernel_size=4, padding='same', activation='relu')(conv2d_4x4_3)

        # 5x5 branch
        conv2d_5x5_1 = Conv2D(filters=50, kernel_size=5, padding='same', activation='relu')(input_concat)
        conv2d_5x5_2 = Conv2D(filters=50, kernel_size=5, padding='same', activation='relu')(conv2d_5x5_1)
        conv2d_5x5_3 = Conv2D(filters=50, kernel_size=5, padding='same', activation='relu')(conv2d_5x5_2)
        conv2d_5x5_4 = Conv2D(filters=50, kernel_size=5, padding='same', activation='relu')(conv2d_5x5_3)

        # First concatenation
        concat_1 = concatenate([conv2d_3x3_4, conv2d_4x4_4, conv2d_5x5_4])

        # Pre-output branching
        conv2d_3x3_final = Conv2D(filters=50, kernel_size=3, padding='same', activation='relu')(concat_1)
        conv2d_4x4_final = Conv2D(filters=50, kernel_size=4, padding='same', activation='relu')(concat_1)
        conv2d_5x5_final = Conv2D(filters=50, kernel_size=5, padding='same', activation='relu')(concat_1)

        # Final concatenation
        concat_final = concatenate([conv2d_3x3_final, conv2d_4x4_final, conv2d_5x5_final])

        # Output
        container = Conv2D(filters=3, kernel_size=1, padding='same',
                           activation='relu', name='container_output')(concat_final)

        return container

    @staticmethod
    def get_reveal_network(container):
        # 3x3 branch
        conv2d_3x3_1 = Conv2D(filters=50, kernel_size=3, padding='same', activation='relu')(container)
        conv2d_3x3_2 = Conv2D(filters=50, kernel_size=3, padding='same', activation='relu')(conv2d_3x3_1)
        conv2d_3x3_3 = Conv2D(filters=50, kernel_size=3, padding='same', activation='relu')(conv2d_3x3_2)
        conv2d_3x3_4 = Conv2D(filters=50, kernel_size=3, padding='same', activation='relu')(conv2d_3x3_3)

        # 4x4 branch
        conv2d_4x4_1 = Conv2D(filters=50, kernel_size=4, padding='same', activation='relu')(container)
        conv2d_4x4_2 = Conv2D(filters=50, kernel_size=4, padding='same', activation='relu')(conv2d_4x4_1)
        conv2d_4x4_3 = Conv2D(filters=50, kernel_size=4, padding='same', activation='relu')(conv2d_4x4_2)
        conv2d_4x4_4 = Conv2D(filters=50, kernel_size=4, padding='same', activation='relu')(conv2d_4x4_3)

        # 5x5 branch
        conv2d_5x5_1 = Conv2D(filters=50, kernel_size=5, padding='same', activation='relu')(container)
        conv2d_5x5_2 = Conv2D(filters=50, kernel_size=5, padding='same', activation='relu')(conv2d_5x5_1)
        conv2d_5x5_3 = Conv2D(filters=50, kernel_size=5, padding='same', activation='relu')(conv2d_5x5_2)
        conv2d_5x5_4 = Conv2D(filters=50, kernel_size=5, padding='same', activation='relu')(conv2d_5x5_3)

        # First concatenation
        concat_1 = concatenate([conv2d_3x3_4, conv2d_4x4_4, conv2d_5x5_4])

        # Pre-output branching
        conv2d_3x3_final = Conv2D(filters=50, kernel_size=3, padding='same', activation='relu')(concat_1)
        conv2d_4x4_final = Conv2D(filters=50, kernel_size=4, padding='same', activation='relu')(concat_1)
        conv2d_5x5_final = Conv2D(filters=50, kernel_size=5, padding='same', activation='relu')(concat_1)

        # Final concatenation
        concat_final = concatenate([conv2d_3x3_final, conv2d_4x4_final, conv2d_5x5_final])

        # Output
        revealed_secret = Conv2D(filters=3, kernel_size=1, padding='same',
                                 activation='relu', name='revealed_secret_output')(concat_final)

        return revealed_secret

    def get_model(self):
        prepared_secret = self.get_preparation_network()
        container = self.get_hiding_network(prepared_secret)
        revealed_secret = self.get_reveal_network(container)
        model = Model(inputs=[self.dct_secret, self.dct_cover], outputs=[container, revealed_secret])
        return model

    def get_losses(self):
        losses = {
            'container_output': 'mean_squared_error',
            'revealed_secret_output': 'mean_squared_error'
        }
        loss_weights = {'container_output': 1.0, 'revealed_secret_output': self.beta}
        return losses, loss_weights

    def train(self, X, y):
        losses, loss_weights = self.get_losses()
        self.model.compile(optimizer='Adam', loss=losses, loss_weights=loss_weights, metrics=['accuracy'])
        history = self.model.fit(X, y, epochs=self.n_epochs)

        return history

    @staticmethod
    def predict(model, data):
        dct_results = model.predict([data['dct_secret'], data['dct_cover']])
        dct_container = dct_results[0]
        dct_revealed_secret = dct_results[1]

        container_result = block_wise_idct(dct_container[0])
        rgb_container_result = ycbcr_to_rgb(container_result)

        revealed_secret_result = block_wise_idct(dct_revealed_secret[0])
        rgb_revealed_secret_result = ycbcr_to_rgb(revealed_secret_result)

        return container_result, rgb_container_result, revealed_secret_result, rgb_revealed_secret_result

    def print_model(self):
        print(self.model.summary())

    def save_model_visualization(self):
        plot_model(self.model, to_file='model_visualization.png', show_shapes=True)

    def save_model(self, i, dir_name):
        # serialize model to JSON
        model_json = self.model.to_json()
        with open(str(dir_name) + '/' + "model.json", "w") as json_file:
            json_file.write(model_json)
        # serialize weights to HDF5
        self.model.save_weights(str(dir_name) + '/' + "model.h5")
        print(f"Saved model to disc. Folder name {dir_name}")

    @staticmethod
    def load_model(i, dir_name):
        json_file = open(str(dir_name) + '/' + 'model.json', 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        loaded_model = model_from_json(loaded_model_json)

        # load weights into new model
        loaded_model.load_weights(str(dir_name) + '/' + "model.h5")
        print("Loaded model from disk")

        return loaded_model
