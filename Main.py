from Model import DCTSteganographyModel
from Preprocessor import Preprocessor
from Transformer import ycbcr_to_rgb
from Utility import check_cmd_args, save_image_to_png, training_data_to_csv, history_to_csv, dir_names_to_csv
import time
import os

NO_MODELS = 10
RESULTS_PATH = 'results'

if __name__ == "__main__":

    DIR_NAMES = []

    args = check_cmd_args()
    n_epochs = args['epochs']
    beta = args['beta']

    # prepare training data
    p = Preprocessor('images/dugong.jpg', 'images/cat.jpg')

    original_images = p.get_original_images()

    rgb_original_secret = ycbcr_to_rgb(original_images['secret'])
    rgb_original_cover = ycbcr_to_rgb(original_images['cover'])

    training_data = p.get_training_data()
    dct_cover = training_data['dct_cover']
    dct_secret = training_data['dct_secret']

    if not os.path.exists(RESULTS_PATH):
        os.mkdir(RESULTS_PATH)

    is_model_saved = False

    for i in range(NO_MODELS):
        model = DCTSteganographyModel(dct_secret, dct_cover, n_epochs, beta)

        if not is_model_saved:
            model.print_model()
            model.save_model_visualization()
            is_model_saved = True

        dir_name = RESULTS_PATH + '/' + 'model' + str(i) + 'epochs' + str(n_epochs) + 'beta' + str(beta)
        dir_name = dir_name.replace('.', '')
        DIR_NAMES.append(dir_name)

        if not os.path.exists(dir_name):
            os.mkdir(dir_name)

        save_image_to_png(rgb_original_secret, 'original_secret', dir_name)
        save_image_to_png(rgb_original_cover, 'original_cover', dir_name)

        print(f'\nTraining model {i}:\n\n')

        start = time.time()
        history = model.train([dct_secret, dct_cover], [dct_cover, dct_secret])
        stop = time.time()

        print(f'\nTraining time: {(stop - start) / 60} min.\n')

        training_data_to_csv(n_epochs, beta, ((stop - start) / 60), dir_name)
        history_to_csv(history.history, dir_name)
        model.save_model(i, dir_name)

        # print(DIR_NAMES)
        dir_names_to_csv(DIR_NAMES)
