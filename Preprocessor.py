import numpy as np
from PIL import Image, ImageOps
from Transformer import block_wise_dct, block_wise_idct, ycbcr_to_rgb
from Utility import DIVIDER
from Utility import display_image, prepare_image, prepare_dct_image


class Preprocessor:

    IMAGE_SIZE = (64, 64)

    def __init__(self, cover_path, secret_path):
        self.cover = self.load_image_to_array(cover_path)
        self.secret = self.load_image_to_array(secret_path)
        self.dct_cover = self.get_image_transform(self.cover)
        self.dct_secret = self.get_image_transform(self.secret)

    def load_image_to_array(self, image_path):
        image = Image.open(image_path)
        return np.array(ImageOps.fit(image, self.IMAGE_SIZE).convert('YCbCr'), dtype=np.float32)

    @staticmethod
    def get_image_transform(image):
        return block_wise_dct(image)

    def get_training_data(self):
        return {
            'dct_secret': np.expand_dims(self.dct_secret, axis=0),
            'dct_cover': np.expand_dims(self.dct_cover, axis=0)
        }

    def get_original_images(self):
        return {
            'secret': self.secret,
            'cover': self.cover
        }


if __name__ == '__main__':

    p = Preprocessor('images/dugong.jpg', 'images/cat.jpg')

    # prepare inverse transforms of dct of original images
    idct_cover = block_wise_idct(p.dct_cover)
    idct_secret = block_wise_idct(p.dct_secret)

    prepare_image(p.cover/DIVIDER, title='Original cover image')
    prepare_image(p.secret/DIVIDER, title='Original secret image')
    prepare_dct_image(p.dct_cover, title='Y channel DCT of the cover image')
    prepare_dct_image(p.dct_secret, title='Y channel DCT of the secret image')
    prepare_image(idct_cover/DIVIDER, title='IDCT - cover image')
    prepare_image(idct_secret/DIVIDER, title='IDCT - secret image')
    prepare_image(ycbcr_to_rgb(idct_cover), 'RGB cover image')
    prepare_image(ycbcr_to_rgb(idct_secret), 'RGB secret image')

    display_image()
