from Utility import DIVIDER
from Utility import prepare_image, display_image, save_image_to_png, dir_names_from_csv, find_image_min_max, \
    scale_image
from Preprocessor import Preprocessor
from Model import DCTSteganographyModel
from Main import NO_MODELS
import numpy as np

DIR_NAMES = dir_names_from_csv()

for i in range(NO_MODELS):
    # load json and create model
    model = DCTSteganographyModel.load_model(i, DIR_NAMES[i])

    p = Preprocessor('images/dugong.jpg', 'images/cat.jpg')
    training_data = p.get_training_data()

    container_result, rgb_container_result, revealed_secret_result, rgb_revealed_secret_result\
        = DCTSteganographyModel.predict(model, training_data)

    # prepare_image(container_result/DIVIDER, 'YCbCr container')
    # prepare_image(rgb_container_result, 'RGB container')
    #
    # prepare_image(revealed_secret_result/DIVIDER, 'YCbCr revealed secret')
    # prepare_image(rgb_revealed_secret_result, 'RGB revealed secret')
    #
    # display_image()

    # YCbCr
    container_min, container_max = find_image_min_max(container_result)
    if container_min == container_max:
        scaled_container = scale_image(container_result, container_min, container_max)
        save_image_to_png(np.uint8(scaled_container), 'ycbcr_container', DIR_NAMES[i])
    else:
        save_image_to_png(np.uint8(container_result), 'ycbcr_container', DIR_NAMES[i])

    revealed_secret_min, revealed_secret_max = find_image_min_max(revealed_secret_result)
    if revealed_secret_min == revealed_secret_max:
        scaled_revealed_secret = scale_image(revealed_secret_result, revealed_secret_min, revealed_secret_max)
        save_image_to_png(np.uint8(scaled_revealed_secret), 'ycbcr_revealed_secret', DIR_NAMES[i])
    else:
        save_image_to_png(np.uint8(revealed_secret_result), 'ycbcr_revealed_secret', DIR_NAMES[i])

    # RGB
    rgb_container_min, rgb_container_max = find_image_min_max(rgb_container_result)
    if rgb_container_min == rgb_container_max:
        rgb_scaled_container = scale_image(rgb_container_result, rgb_container_min, rgb_container_max)
        save_image_to_png(np.uint8(rgb_scaled_container), 'rgb_container', DIR_NAMES[i])
    else:
        save_image_to_png(np.uint8(rgb_container_result), 'rgb_container', DIR_NAMES[i])

    rgb_revealed_secret_min, rgb_revealed_secret_max = find_image_min_max(rgb_revealed_secret_result)
    if rgb_revealed_secret_min == rgb_revealed_secret_max:
        rgb_scaled_revealed_secret = scale_image(rgb_revealed_secret_result, rgb_revealed_secret_min,
                                                 rgb_revealed_secret_max)
        save_image_to_png(np.uint8(rgb_scaled_revealed_secret), 'rgb_revealed_secret', DIR_NAMES[i])
    else:
        save_image_to_png(np.uint8(rgb_revealed_secret_result), 'rgb_revealed_secret', DIR_NAMES[i])
