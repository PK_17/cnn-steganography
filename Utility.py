import argparse
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import csv

# constants
DIVIDER = 255
DCT_SCALE = 0.01


def display_image():
    plt.show()


def save_image_to_png(image, name, dir_name):
    image = Image.fromarray(image)
    image.save(str(dir_name) + '/' + name + '.png')
    print('RGB result has been saved to the disk')


def find_image_min_max(image):
    return np.amin(image), np.amax(image)


def scale_image(image, min, max):
    return np.around((image - min) * (DIVIDER / (max - min)))


def prepare_image(image, title='Image'):
    plt.figure()
    plt.suptitle(title)
    plt.imshow(image)


def prepare_dct_image(dct_image, title='DCT image Y channel'):
    plt.figure()
    plt.suptitle(title)
    plt.imshow(dct_image[:, :, 0], cmap='gray', vmax=np.max(dct_image[:, :, 0])*DCT_SCALE, vmin=0)


def check_cmd_args():
    ap = argparse.ArgumentParser()
    ap.add_argument('-e', '--epochs', required=True, type=int,
                    help='specify number of epochs')
    ap.add_argument('-b', '--beta', required=False, default=0.5, type=float,
                    help='coefficient beta regulating revealed secret quality importance')
    return vars(ap.parse_args())


def training_data_to_csv(n_epochs, beta, training_time, dir_name):
    with open(str(dir_name) + '/' + 'training_data.csv', mode='w', newline='') as csv_file:
        fieldnames = ['epochs', 'beta', 'training_time']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

        writer.writeheader()
        writer.writerow({
            'epochs': n_epochs,
            'beta': beta,
            'training_time': training_time
        })


def training_data_from_csv(dir_name):
    n_epochs, beta = 0, 0
    with open(str(dir_name) + '/' + 'training_data.csv', newline='') as csv_file:
        fieldnames = ['epochs', 'beta']
        reader = csv.DictReader(csv_file, fieldnames=fieldnames)
        for row in reader:
            n_epochs = row['epochs']
            beta = row['beta']
    return n_epochs, beta


def results_to_csv(n_epochs, beta, ssim_hide, ssim_reveal, dir_name):
    with open(str(dir_name) + '/' + 'ssim_results.csv', mode='w', newline='') as csv_file:
        fieldnames = ['epochs', 'beta', 'ssim_hide', 'ssim_reveal']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

        writer.writeheader()
        writer.writerow({
            'epochs': n_epochs,
            'beta': beta,
            'ssim_hide': ssim_hide,
            'ssim_reveal': ssim_reveal
        })


def history_to_csv(history, dir_name):
    keys = sorted(history.keys())
    with open(str(dir_name) + '/' + "history.csv", mode='w', newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter="\t")
        writer.writerow(keys)
        writer.writerows(zip(*[history[key] for key in keys]))


def dir_names_to_csv(dir_names):
    with open('dir_names.csv', mode='w', newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter="\t")
        for directory in dir_names:
            writer.writerow([directory])


def dir_names_from_csv():
    dir_names = []
    with open('dir_names.csv', mode='r', newline='') as csv_file:
        reader = csv.reader(csv_file, delimiter="\t")
        for row in reader:
            dir_names.append(row[0])

    return dir_names
