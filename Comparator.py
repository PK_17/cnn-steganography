from skimage.measure import compare_ssim as ssim
import numpy as np
from PIL import Image, ImageOps
from Preprocessor import Preprocessor
from Main import NO_MODELS
from Utility import dir_names_from_csv, training_data_from_csv, results_to_csv


def load_image_to_array(image_path):
    image = Image.open(image_path)
    return np.array(ImageOps.fit(image, Preprocessor.IMAGE_SIZE).convert('RGB'), dtype=np.float32)


DIR_NAMES = dir_names_from_csv()

for i in range(NO_MODELS):
    original_secret = load_image_to_array(str(DIR_NAMES[i]) + '/' + 'original_secret.png')
    original_cover = load_image_to_array(str(DIR_NAMES[i]) + '/' + 'original_cover.png')

    container = load_image_to_array(str(DIR_NAMES[i]) + '/' + 'rgb_container.png')
    revealed_secret = load_image_to_array(str(DIR_NAMES[i]) + '/' + 'rgb_revealed_secret.png')

    ssim_hide = ssim(original_cover, container, multichannel=True)
    ssim_reveal = ssim(original_secret, revealed_secret, multichannel=True)

    n_epochs, beta = training_data_from_csv(DIR_NAMES[i])
    results_to_csv(n_epochs, beta, ssim_hide, ssim_reveal, DIR_NAMES[i])
