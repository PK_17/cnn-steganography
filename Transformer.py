from scipy.fftpack import dct, idct
import numpy as np

# constants
WINDOW_SIZE = 4
CHANNELS = 3
CHANNEL_SHIFT = 128


def dct_2d(image):
    return dct(dct(image, axis=0, norm="ortho"), axis=1, norm="ortho")


def idct_2d(dct_image):
    return idct(idct(dct_image, axis=0, norm="ortho"), axis=1, norm="ortho")


def block_wise_dct(image, window_size=WINDOW_SIZE):
    im_size = image.shape
    dct_image = np.zeros(im_size)

    for image_channel in range(CHANNELS):
        for row in np.r_[:im_size[0]:window_size]:
            for col in np.r_[:im_size[1]:window_size]:
                dct_image[row:(row + window_size), col:(col + window_size), image_channel] = dct_2d(
                    image[row:(row + window_size), col:(col + window_size), image_channel])

    return dct_image


def block_wise_idct(dct_image, window_size=WINDOW_SIZE):
    im_size = np.array(dct_image.shape)
    idct_image = np.zeros(im_size)

    for dct_image_channel in range(CHANNELS):
        for row in np.r_[:im_size[0]:window_size]:
            for col in np.r_[:im_size[1]:window_size]:
                idct_image[row:(row + window_size), col:(col + window_size), dct_image_channel] = idct_2d(
                    dct_image[row:(row + window_size), col:(col + window_size), dct_image_channel])

    return idct_image


def ycbcr_to_rgb(ycbcr_image):

    multiplier = np.array([[1, 0, 1.402], [1, -0.34414, -.71414], [1, 1.772, 0]])

    rgb_image = np.array(ycbcr_image).astype(np.float32)
    rgb_image[:, :, [1, 2]] -= CHANNEL_SHIFT
    rgb_image = rgb_image.dot(multiplier.T)
    np.putmask(rgb_image, rgb_image > 255, 255)
    np.putmask(rgb_image, rgb_image < 0, 0)

    return np.uint8(rgb_image)
